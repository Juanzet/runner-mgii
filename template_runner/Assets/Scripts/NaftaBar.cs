﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NaftaBar : MonoBehaviour
{
    public Image barraNafta;
    public float naftaMaxima;
    public float naftaActual;
     
    void Update()
    {
        barraNafta.fillAmount = naftaActual / naftaMaxima;
    }
}
