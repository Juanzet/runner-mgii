﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUp : MonoBehaviour
{
    public float puSpeed;
    float yVariation;
    public float yFreq;
    Rigidbody _rigidbody;


    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        yVariation = Mathf.Sin(Time.time * yFreq);
        transform.position += (Vector3.right*-puSpeed + Vector3.up*yVariation) * Time.deltaTime;
    }

    public void OutOfBounds() //Si esta fuera del rango de vision del jugador, destruye al objeto
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }

   

}
