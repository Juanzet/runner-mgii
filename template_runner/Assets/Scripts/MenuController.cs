﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuController : MonoBehaviour
{
     
    public void Exit()
    {
        Application.Quit();
    }

    public void Play()
    {
        SceneManager.LoadScene("RunnerScene");
    }
}
