﻿using System.Collections;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    public static int power = 0;
    private bool floored;

    public static bool inWater;

    private void Start()
    {
        AudioManager.instance.Play("SonidoJuego");
        rb = GetComponent<Rigidbody>(); //Obtiene el rigidbody del objeto y sus componentes
        initialSize = rb.transform.localScale.y; //Define el tamaño inicial del personaje
        power = 0;
    }

    void Update()
    {
        GetInput();//Ejecuta el input y la accion que realiza el personaje
    }

    private void GetInput()
    {
        Jump();
        Duck();
    }

    private void Jump()
    {
        if (floored)//Hace que el personaje salta con la tecla "W", si el personaje se encuentra en el piso
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Duck()
    {
        if (floored)//Si se encuentra en el piso, cambia la escala del personaje con la tecla S, convirtiendolo de un rectangulo a un cubo
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else //Si la escala del personaje es distinta a la escala inicial, lo devuelve a su escala inicial
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))//Le agrega una fuerza hacia el piso, si el objeto se encuentra en el aire
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && power != 2)//Si el personaje se choca con un enemigo, se destruye al jugador y aparece la pantalla de game over
        {
            AudioManager.instance.Stop("SonidoJuego");
            AudioManager.instance.Play("PlayerDead");
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            Controller_Hud.gameOver = true;
            
        }else if(collision.gameObject.CompareTag("Enemy") && power == 1)
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("Floor"))//Si el personaje se encuenta en el piso, la variable "floored" es verdadera
        {
            floored = true;
        }

        if(collision.gameObject.CompareTag("Nafta")) 
        {
            AudioManager.instance.Play("PowerUp");
            Controller_Hud.naftaActual = 25;
            Controller_Instantiator.naftaAmount = 0;
            Destroy(collision.gameObject);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))//Si el personaje se encuentra en el aire, la variable "floored" es falsa
        {
            floored = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Power"))
        {
            power = Random.Range(1, 3);
            Destroy(other.gameObject);
            StartCoroutine(FinishPower());
        }

        if (other.gameObject.CompareTag("Water"))
        {
            inWater = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("power"))
        {
            inWater = false;
        }
    }


    IEnumerator FinishPower()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        power = 0;
    }
}
