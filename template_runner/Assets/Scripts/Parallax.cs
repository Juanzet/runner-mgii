﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        startPos = transform.position.x; //define la posicion inicial, siendo la misma la posicion x del objeto
        length = GetComponent<SpriteRenderer>().bounds.size.x; //Obtiene el tamaño del spriterenderer del objeto, en su ejex
    }

    void Update()
    {

        if (Controller_Hud.gameOver == false)//Evalua si se esta jugando o no, si esta en game over la imagen deja de moverse
        {
            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z); //Mueve el sprite en sentido contrario al jugador
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);//Reinicia la posicion del sprite, si su posicion es menor a -20
            }
        }
    }
}
