﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public GameObject instantiatePosNafta;
    public List<GameObject> nafta;
    public List<GameObject> power;
    public List<GameObject> Water;
    public float respawningTimer;
    private float time = 0;

    public static int naftaAmount = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2; //define la velocidad del enemigo utilizando una instancia de la clase "Controller_Enemy"
        naftaAmount = 0;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
        SpawnNafta();
        SpawnPowerUp();
        SpawnWater();
    }

    private void ChangeVelocity()//Suaviza la velocidad del enemigo
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()//Spawnea a los enemigos de manera random
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }

    private void SpawnNafta()
    {
        respawningTimer -= Time.deltaTime;
        if (respawningTimer <= 0 && naftaAmount == 0)
        {
            Instantiate(nafta[Random.Range(0, nafta.Count)], instantiatePosNafta.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
            naftaAmount++;
        }
    }

    private void SpawnPowerUp()
    {
        respawningTimer -= Time.deltaTime;
        if (respawningTimer <= 0 && naftaAmount == 0)
        {
            Instantiate(power[Random.Range(0, power.Count)], instantiatePosNafta.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
            naftaAmount++;
        }
    }

    private void SpawnWater()
    {
        respawningTimer -= Time.deltaTime;
        if (respawningTimer <= 0 && naftaAmount == 0)
        {
            Instantiate(Water[Random.Range(0, Water.Count)], instantiatePosNafta.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
            naftaAmount++;
        }
    }
}
