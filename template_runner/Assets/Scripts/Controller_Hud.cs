﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text bestDistanceText;
    public Text gameOverText;
    public Text powerUpsText;
    public Image naftaBar;
    public Image WOI;
    public float naftaMaxima;
    public static float naftaActual;

    private float distance = 0;
    public static float finalDistance = 0;
    float decimalDistance;

    void Start()
    {
        naftaActual = naftaMaxima;
        gameOver = false; //Bool iniciado falso para que el juego no se termine
        distance = 0;//valor inicial de la distancia = 0
        decimalDistance = 0;
        finalDistance = DataPersistencia.puntuacionMaxima;
        distanceText.text = distance.ToString();// muestra la distancia inicial en string
        gameOverText.gameObject.SetActive(false);// Desactiva el texto de "Game Over"
        bestDistanceText.gameObject.SetActive(false);
        powerUpsText.gameObject.SetActive(false);
        WOI.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)//Si se pierde en el juego, se muestra la distancia total lograda
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString();
            gameOverText.gameObject.SetActive(true);
            bestDistanceText.text = "Best distance: " + finalDistance.ToString();
            bestDistanceText.gameObject.SetActive(true);
            if(distance > finalDistance)
            {
                finalDistance = distance;
            }
        }
        else //Te muestra la distancia
        {
            LogicaNafta();
            distance += Time.deltaTime;
            decimalDistance += 1 * Time.deltaTime;

            distance = Mathf.RoundToInt(decimalDistance);
            distanceText.text = distance.ToString();

        }

        if(Controller_Player.power == 1)
        {
            powerUpsText.gameObject.SetActive(true);
            powerUpsText.text = "Infinite fuel!";
            powerUpsText.color = Color.red;
            naftaBar.color = Color.blue;
        }else if(Controller_Player.power == 2)
        {
            powerUpsText.gameObject.SetActive(true);
            powerUpsText.text = "Inmunity!";
            powerUpsText.color = Color.red;
        }
        else if(Controller_Player.power == 3)
        {
            powerUpsText.gameObject.SetActive(true);
            powerUpsText.text = "Double points!";
            powerUpsText.color = Color.red;
            distance *= 2;
        }
        else
        {
            powerUpsText.gameObject.SetActive(false);
            naftaBar.color = Color.green;
        }

        if(Controller_Player.inWater == true)
        {
            WOI.gameObject.SetActive(true);
        }
        else
        {
            WOI.gameObject.SetActive(false);
        }

    }


    public void LogicaNafta()
    {
        if (Controller_Player.power != 1)
        {
            naftaBar.fillAmount = naftaActual / naftaMaxima;

            naftaActual -= Time.deltaTime;

            if (naftaBar.fillAmount == 0)
            {
                gameOver = true;
            }
        }
    }

    

}
