﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); //Obtiene el rigidbody del objeto y sus componentes
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force); // Enemigo se mueve en sentido contrario al jugador
        OutOfBounds();
    }

    public void OutOfBounds() //Si esta fuera del rango de vision del jugador, destruye al objeto
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
